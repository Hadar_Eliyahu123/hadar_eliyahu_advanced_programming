#include <iostream>
#include <string>

using std::string;

class myString
{
private:
	string* _strPtr;

public:
	void init(string str); // insert the string
	string getStr(); // returns the string
	int getLength(); // returns the length of the string
	char getCharUsingSquareBrackets(int index); // returns char by index
	char getCharUsingRoundBrackets(int index); // returns char by index
	char getFirstChar(); // returns the first char
	char getLastChar(); // returns the last char
	int getCharFirstOccurrenceInd(char c); // returns the first appearence of char is the string
	int getCharLastOccurrenceInd(char c); // returns the last appearence of char is the string
	int gstStringFirstOccurrenceInd(string str); // returns the first appearence of full string in the string
	bool isEqual(string s); // compare the string with other string
};
