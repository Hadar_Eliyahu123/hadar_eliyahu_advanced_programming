#include <string>
#include <iostream>
#include "Course.h"
using namespace std;

// initial course
void Course::init(string name, int test1, int test2, int exam)
{
	Coursename = name;
	test_1 = test1;
	test_2 = test2;
	exam_test = exam;
}

// returns all grades
int* Course::getGradesList()
{
	int grades[3] = {0};
	grades[0] = test_1;
	grades[1] = test_2;
	grades[2] = exam_test;
	return grades;
}

// returns course name
string Course::getName()
{
	return Coursename;
}
// returns final grade of the course
double Course::getFinalGrade()
{
	return test_1 * 0.25 + test_2 * 0.25 + exam_test * 0.5;
}