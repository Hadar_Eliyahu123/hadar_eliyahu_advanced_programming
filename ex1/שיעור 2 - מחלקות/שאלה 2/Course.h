#ifndef COURSE_H
#define COURSE_H
#include <iostream>
#include <string>
using namespace std;
//Representation of a Course grade-book
//test - 25%
//final exam - 50%

//can only change grades after creation.
class Course
{
public:
	//Methods
	void init(string name, int test1, int test2, int exam);

	int* getGradesList();
	string getName();
	double getFinalGrade();


private: //what fields do you need?
	string Coursename;
	int test_1;
	int test_2;
	int exam_test;
};

#endif