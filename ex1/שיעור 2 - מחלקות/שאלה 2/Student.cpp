#include <iostream>
#include "Student.h"

// initail student
void Student::init(string name, Course** courses, int crsCount)
{
	student_name = name;
	CoursesList = courses;
	numOfCourses = crsCount;
}

// returns list of courses
Course** Student::getCourses()
{
	return CoursesList;
}

// returns how many courses the student studies in
int Student::getCrsCount()
{
	return numOfCourses;
}

// returns student's average grade
double Student::getAvg()
{
	double avg = 0;
	for (int i = 0; i < numOfCourses; i++)
	{
		avg += CoursesList[i]->getFinalGrade();
	}
	return (avg / numOfCourses);
}

// changes the name of the student
void Student::setName(string name)
{
	student_name = name;
}

// returns student's name
string Student::getName()
{
	return student_name;
}