#ifndef STUDENT_H
#define STUDENT_H
#include <string>
#include <iostream>
#include "Course.h"

using namespace std;

//Represents a student's Courses and total average
//MAGIC_BONUS is: 1 for any Course that ends with 'y'
//				  2 for any Course that starts with 'e'
//				  5 for any Course that ends with 'h'
class Student
{
public:
	void init(string name, Course** courses, int crsCount);
	string getName();
	void setName(string name);
	int getCrsCount();
	Course** getCourses();
	double getAvg();


private: //what fields do you need?
	Course** CoursesList;
	string student_name;
	int numOfCourses;
};


#endif