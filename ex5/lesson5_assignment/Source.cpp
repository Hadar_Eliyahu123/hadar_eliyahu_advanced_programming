#include "Helper.h"
#include <windows.h>
#include <iostream>

typedef int(__cdecl *MYPROC)(LPWSTR);

using namespace std;
string ExePath();
int main(void)
{
	WIN32_FIND_DATA FindFileData;
	string command;
	HINSTANCE hinstLib;
	MYPROC ProcAdd;
	BOOL fFreeResult, fRunTimeLinkSuccess = FALSE;
	vector < string > v;
	while (true)
	{
		cout << ">>";
		getline(cin, command);
		Helper::trim(command);
		v = Helper::get_words(command);
		if (v[0] == "pwd")
		{
			cout << ExePath();
		}
		if (v[0] == "cd")
		{
			const char * c = v[1].c_str();
			if (!SetCurrentDirectory(c))
			{
				printf("SetCurrentDirectory failed (%d)\n", GetLastError());
			}
		}
		if (v[0] == "create")
		{
			CreateFile(v[1].c_str(), FILE_READ_DATA, FILE_SHARE_READ,
				NULL, OPEN_ALWAYS, 0, NULL);
		}
		if (v[0] == "ls")
		{
			
			HANDLE hFind = FindFirstFile(ExePath().c_str(), &FindFileData);

			if (INVALID_HANDLE_VALUE != hFind)
			{
				FindNextFile(hFind, &FindFileData);
				FindNextFile(hFind, &FindFileData);
				do
				{
					cout << FindFileData.cFileName << endl;

				} while (FindNextFile(hFind, &FindFileData) != 0);
				FindClose(hFind);
			}
		}
		if (v[0] == "secret")
		{
			hinstLib = LoadLibrary("secret.dll");

			if (hinstLib != NULL)
			{
				ProcAdd = (MYPROC)GetProcAddress(hinstLib, "TheAnswerToLifeTheUniverseAndEverything");

				// If the function address is valid, call the function.

				if (NULL != ProcAdd)
				{
					fRunTimeLinkSuccess = TRUE;
					(ProcAdd)(L"Message sent to the DLL function\n");
				}
				// Free the DLL module.

				fFreeResult = FreeLibrary(hinstLib);
			}
		}
		if (v[0] == "exe")
		{
			STARTUPINFO si;
			PROCESS_INFORMATION pi;
			CreateProcess(v[1].c_str(), NULL, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &si, &pi);
			WaitForSingleObject(pi.hProcess, INFINITE);
			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);
		}

		cout << endl;
	}

	system("pause");
	return 0;
}


string ExePath() {
	char buffer[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, buffer);
	string::size_type pos = string(buffer).find_last_of("\\/");
	return string(buffer);
}