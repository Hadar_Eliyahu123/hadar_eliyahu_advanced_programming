﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _21_last
{
    public partial class MainApp : Form
    {
        string _username; 
        public MainApp(string name)
        {
            _username = name;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = _username;
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string date = monthCalendar1.SelectionRange.Start.Month.ToString() + "/" + 
                          monthCalendar1.SelectionRange.Start.Day.ToString() + "/" +
                          monthCalendar1.SelectionRange.Start.Year.ToString();


            string name = "guest";
            bool found = false;
            try
            {
                StreamReader sr = new StreamReader(_username + "BD.txt");
                string str;
                while ((str = sr.ReadLine()) != null && !found)
                {
                    if (str.Substring(str.IndexOf(',') + 1, str.Length - str.IndexOf(',') - 1).Equals(date))
                    {
                        name = str.Substring(0, str.IndexOf(','));
                        found = true;
                    }
                }
                sr.Close();
                if (!found)
                {
                    label2.Text = "אף אחד לא חוגג בתאריך הנבחר";
                }
                if (found)
                {
                    label2.Text = "בתאריך הנבחר - " + name + "חוגג יום הולדת"; 
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("File Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
