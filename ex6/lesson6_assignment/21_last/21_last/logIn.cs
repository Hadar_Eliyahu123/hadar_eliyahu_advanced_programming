﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _21_last
{
    public partial class logIn : Form
    {
        public logIn()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = "guest";
            bool found = false;
            try
            {
                StreamReader sr = new StreamReader("Users.txt");
                string str;
                while ((str = sr.ReadLine()) != null && !found)
                {
                    if (str.Substring(0, str.IndexOf(',')).Equals(textBox1.Text))
                    {

                        if (str.Substring(str.IndexOf(',') + 1, str.Length - str.IndexOf(',') - 1).Equals(textBox2.Text))
                        {
                            username = str.Substring(0, str.IndexOf(','));
                            found = true;
                        }
                    }
                }
                sr.Close();
                if(!found)
                {
                    MessageBox.Show("User Name or Password are not correct... :(", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if(found)
                {
                    MainApp f = new MainApp(username);
                    this.Hide();
                    f.ShowDialog();
                    this.Close();
                }
            }
            catch(Exception err)
            {
                MessageBox.Show("File Not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
        }
    }
}
