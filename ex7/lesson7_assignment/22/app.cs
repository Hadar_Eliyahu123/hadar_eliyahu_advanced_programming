﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;

namespace _22
{
    public partial class app : Form
    {
        bool run = true;
        string card = "";
        public app()
        {
            InitializeComponent();
            generateCards();

            Thread ser = new Thread(new ThreadStart(serve));

            ser.Start();
            while (!ser.IsAlive) ;

            while (card == "") ;

            ser.Abort();
            ser.Join();
            MessageBox.Show(card, "card", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public void generateCards()
        {
            int x = 0;
            PictureBox[] picture = new PictureBox[10];
            for (int i = 0; i < 10; i++)
            {
                picture[i] = new PictureBox
                {
                    Name = i.ToString(),
                    Size = new Size(120, 250),
                    SizeMode = PictureBoxSizeMode.StretchImage,
                    Location = new Point(x, 100),
                    Visible = true,
                    Tag = 0
                };
                picture[i].ImageLocation = @"PNG-cards/card back blue.png";
                picture[i].Click += flip;
                x += 138;
                this.Controls.Add(picture[i]);
            }

            
        }

        public void flip(object sender, EventArgs e)
        {
            PictureBox b = (PictureBox)sender;
            if(run)
            {
                if(b.ImageLocation == @"PNG-cards/card back red.png")
                {
                    b.ImageLocation = @"PNG-cards/card back blue.png";
                }
            }
            else
            {
                run = true;
            }
        }
        
        public void randomCard()
        {

        }

        public void serve()
        {
            TcpClient t = new TcpClient("127.0.0.1", 8820);
            NetworkStream stream = t.GetStream();
            Byte[] data = new Byte[256];
            Int32 bytes = stream.Read(data, 0, data.Length); 
            card = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            stream.Close();
            t.Close();
        }
    }
}
